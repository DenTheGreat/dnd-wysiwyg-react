import React from 'react';
import TextEditor from '../Editor/Editor';
import draftToHtml from 'draftjs-to-html';
import ReactHtmlParser from 'react-html-parser';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import 'materialize-css'
import './App.sass'

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

class App extends React.Component{

  state = {
    blocks: []
  }

  addBlock = (block) => {
    this.setState(state => ({blocks:[...state.blocks, block]}))
  }

  onDragEnd = (result) => {
    console.log(result)
    if (!result.destination) {
      return;
    }

    const blocks = reorder(
      this.state.blocks,
      result.source.index,
      result.destination.index
    );

    this.setState({
      blocks,
    });
  }

  save = () => {
    if (this.state.blocks.length > 0) {
      localStorage.setItem('blocks', JSON.stringify(this.state.blocks))
    } else {
      alert("You have nothing to save")
    }
  }

  load = () => {
    const saved = localStorage.getItem('blocks')
    if (saved) {
      this.setState({blocks:JSON.parse(saved)})
    } else {
      alert("You have nothing to load")
    }
  }

  clear = () => {
    localStorage.clear()
    this.setState({blocks:[]})
  }

  render(){
    const {blocks} = this.state;
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
      <div className="wrapper">
        <h1>Drag and Drop Wysiwyg blocks</h1>
        <TextEditor addBlock={this.addBlock}/>
          <Droppable droppableId="droppable" direction="horizontal">
            {(provided) => (
              <div 
                ref={provided.innerRef}
                className="blocks"
                {...provided.droppableProps}>
              {
                blocks.map(
                  (block, index) => (
                    <Draggable key={block.id} draggableId={block.id} index={index}>
                      {(provided) => (
                        <div className="card"
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}>
                          {ReactHtmlParser(draftToHtml(block.item))}
                        </div>
                      )}
                    </Draggable>
                  )
                )
              }
            </div>)}
          </Droppable>
          <div className="btn-group">
            <button className='btn' onClick={this.save}>
                Save
            </button>
            <button className='btn' onClick={this.load}>
                Load
            </button>
            <button className='btn' onClick={this.clear}>
                Clear
            </button>
          </div>
      </div>
      </DragDropContext>
    );
  }
}
export default App;
