import React, { Component } from 'react';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import './Editor.scss'

function getRandomInt(max=10000) {
    return Math.floor(Math.random() * Math.floor(max));
}

class TextEditor extends Component {
    state = {
        editorState: EditorState.createEmpty(),
      }

    onEditorStateChange = (editorState) => {
        this.setState({
        editorState,
        });
    };

    getData = () => {
        this.props.addBlock(
            {
                type: "text",
                id: getRandomInt().toString(),
                item: convertToRaw(this.state.editorState.getCurrentContent())
            }
        )

        const editorState = EditorState.push(this.state.editorState, ContentState.createFromText(''))
        this.setState({ editorState })
    }

    render() {
        const { editorState } = this.state;
        return (
        <div className='editor'>
            <Editor
                toolbar={{
                    options: ['inline'],
                    inline:{
                        options:['bold', 'italic', 'underline']
                    }
                }}
                editorState={editorState}
                wrapperClassName="demo-wrapper"
                editorClassName="demo-editor"
                onEditorStateChange={this.onEditorStateChange}
            />
            <button className="btn" onClick={this.getData}>Submit</button>
        </div> 
        )
    }
}

export default TextEditor